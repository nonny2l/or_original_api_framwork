﻿using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace ORApiFramework.HTTP.Response
{
    public class HttpManageResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        private static HttpManageResponse httpManageResponse;

        public HttpResponseMessage ResponseMessage(HttpRequestMessage request, StatusCode statusCode, string statusMessage, object data)
        {
            HttpResponseMessage response;

            this.Code = (int)statusCode;
            this.Message = statusMessage;

            response = request.CreateResponse(HttpStatusCode.OK, data);
            response.Headers.Add("success", "true");
            response.Headers.Add("code", this.Code.ToString());
            response.Headers.Add("message", this.Message);

            return response;
        }

        public HttpResponseMessage ResponseResultList(HttpRequestMessage request, object dataResponse, ModelStateDictionary modelState, object model, string ModuleName, string methodName)
        {
            HttpResponseMessage response;
            if (modelState.IsValid)
            {

                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.OK, StatusCode.OK.Value(), dataResponse);
                 SaveLog.SaveLogObject(dataResponse, model, ModuleName, methodName);//save log output

            }
            else
            {
                var erorMessage = ErrorMessageManagement.ErrorMessageUtil.getErorModelState(modelState);
                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.EROR, erorMessage, this);
                SaveLog.SaveLogObject(dataResponse, model, ModuleName, methodName);

            }
            return response;
        }


        public static HttpManageResponse Response
        {
            get { return httpManageResponse = new HttpManageResponse(); }
        }


    }

}
