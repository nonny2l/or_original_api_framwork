﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORApiFramework
{
    public class SaveLog
    {
        public static bool SaveLogObject(object responseMessage, object requestMessage, string ModuleType, string ActionType)
        {
            string request = string.Empty;
            string response = JsonConvert.SerializeObject(responseMessage);
            JObject responseJson = JObject.Parse(response);

            if (requestMessage != null)
                request = JsonConvert.SerializeObject(requestMessage);

            //using (var db = new HopsDbContext())
            //{
            //    LOG_API mylog = new LOG_API();
            //    mylog.TRANSACTION_ID = mjson.GetValue("transactionId") == null ? "" : mjson.GetValue("transactionId").ToString();
            //    mylog.ACTION_TYPE = ActionType;
            //    mylog.MODULE_TYPE = ModuleType;
            //    mylog.CREATE_BY = "";
            //    mylog.CREATE_DATE = DateTime.Now;
            //    mylog.REQUEST = datas.ToString();
            //    mylog.REQUEST_DATETIME = mjson.GetValue("transactionDateTime") != null ? Convert.ToDateTime(mjson.GetValue("transactionDateTime").ToString()) : DateTime.Now;
            //    mylog.RESPONSE = JsonConvert.SerializeObject(trn);
            //    mylog.RESPONSE_DATETIME = DateTime.Now;
            //    mylog.STATUS = mjson.GetValue("code") != null ? mjson.GetValue("code").ToString() : "";

            //    db.LOG_API.Add(mylog);
            //    db.SaveChanges();
            //}
            return true;
        }
    }
}