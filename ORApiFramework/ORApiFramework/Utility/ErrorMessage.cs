﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ORApiFramework.Utility
{
    public static class ErrorMessage
    {
        /// <summary>
        /// Get a exception error message.
        /// </summary>
        /// <param name="exception">
        /// The exception that is the cause of the current exception, or a null reference
        /// (Nothing in Visual Basic) if no exception is specified.
        /// </param>
        /// <returns>The error message infomation.</returns>
        public static string ErrorException(this Exception exception)
        {
            string result = string.Empty;
            try
            {
                var errorInfo = string.Empty;
                try
                {
                    StackFrame stackFrame = new StackFrame(1, true);
                    string[] temp = stackFrame.GetFileName().Split('\\');
                    string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
                    string functionName = stackFrame.GetMethod().Name;
                    int fileLineNumber = stackFrame.GetFileLineNumber();
                    int fileColumnNumber = stackFrame.GetFileColumnNumber();
                    string className = stackFrame.GetMethod().DeclaringType.Name;
                    string[] stack = exception.StackTrace.Split('\n');

                    //errorInfo += Environment.NewLine;
                    errorInfo += "///* Comment *///";
                    errorInfo += Environment.NewLine;
                    errorInfo += "File Name : " + fileClassName + Environment.NewLine;
                    errorInfo += "Class Name : " + className + Environment.NewLine;
                    errorInfo += "Method Name : " + functionName + Environment.NewLine;
                    errorInfo += "Line Number : " + fileLineNumber + Environment.NewLine;
                    errorInfo += "Column Number : " + fileColumnNumber + Environment.NewLine; ;
                    errorInfo += "////////////////";
                }
                catch
                { }

                var entityException = exception.GetType();
                if (entityException == typeof(DbEntityValidationException))
                {
                    result += EntityErrorException(exception);
                }
                else
                {
                    if (exception.InnerException != null)
                    {
                        if (exception.InnerException.InnerException != null)
                        {
                            if (exception.InnerException.InnerException.InnerException != null)
                                result += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                            else
                                result += exception.InnerException.InnerException.Message + Environment.NewLine;
                        }
                        else
                        {
                            result += exception.InnerException.Message + Environment.NewLine;
                        }
                    }
                    else
                    {
                        result += exception.Message + Environment.NewLine;
                    }
                }

                result += errorInfo;
                /////
            }
            catch (Exception e)
            {
                if (exception.Message != null)
                {
                    result = exception.Message;
                }
                else
                {
                    result = e.ToString();
                }
            }

            return result;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }
    }
}