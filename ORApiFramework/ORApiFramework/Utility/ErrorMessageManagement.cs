﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace ORApiFramework.Utility
{
    public class ErrorMessageManagement
    {
        public string username = "";
        public string password { get; set; }
        public string email { get; set; }
        public string message { get; set; }
        private static ErrorMessageManagement errorMessageUtil;

        public string getErorModelState(ModelStateDictionary modelStateDictionary)
        {
            foreach (var state in modelStateDictionary)
            {
                foreach (var error in state.Value.Errors)
                {
                    this.message += "," + error.ErrorMessage;
                }
            }
            return this.message;
        }

        public static ErrorMessageManagement ErrorMessageUtil
        {
            get { return errorMessageUtil = new ErrorMessageManagement(); }
        }

    }


}