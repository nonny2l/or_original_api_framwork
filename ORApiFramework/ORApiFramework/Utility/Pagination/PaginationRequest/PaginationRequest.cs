﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORApiFramework.Utility.Pagination
{
    public class PaginationRequest
    {
        public int currentPage { get; set; }
        public int limitPerPage { get; set; }

        public PaginationRequest()
        {
            currentPage = 0;
            limitPerPage = 10;
        }
    }
}
