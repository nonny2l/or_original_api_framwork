﻿using JWT;
using JWT.Builder;
using Newtonsoft.Json;
using ORApiFramework;
using ORApiFramework.HTTP.Response;
using ORApiFramework.Security;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ORApiFramework
{
    public class TokenValidationHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response;
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            string apiLogin = WebConfigurationManager.AppSettings["API_LOGIN"].ToString();
            if (request.RequestUri.AbsolutePath == apiLogin)
            {
                return response = await base.SendAsync(request, cancellationToken);
            }
            else
            {

                long utcDateTimeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                AuthenticationHeaderValue authorization = request.Headers.Authorization;
                if (authorization != null)
                {
                    try
                    {
                        string authParam = authorization.Parameter;
                        IDateTimeProvider provider = new UtcDateTimeProvider();
                        var now = provider.GetNow();

                        var secret = WebConfigurationManager.AppSettings["TOKEN_SECRET_KEY"].ToString();

                        var jwtDeserialize = new JwtBuilder()
                                   .WithSecret(secret)
                                   .MustVerifySignature()
                                   .Decode(authParam);

                        UserAccess userAccess = JsonConvert.DeserializeObject<UserAccess>(jwtDeserialize);
                        if (WebConfigurationManager.AppSettings["ENABLE_EXPIRE_TOKEN"].ToString() == "Y")
                        {
                            if (utcDateTimeNow > userAccess.TokenDateTime)
                            {
                                actionResultStatus.code = (int)HttpStatusCode.RequestTimeout;
                                actionResultStatus.data = null;
                                actionResultStatus.message = "Token has expired";
                                actionResultStatus.description = null;

                                return response = request.CreateResponse(HttpStatusCode.RequestTimeout, actionResultStatus);
                            }
                        }

                        var userNameClaim = new Claim(ClaimTypes.Name, userAccess.UserName);
                        var identity = new ClaimsIdentity(new[] { userNameClaim }, authorization.Scheme);
                        var principal = new ClaimsPrincipal(identity);
                        Thread.CurrentPrincipal = principal;
                        if (System.Web.HttpContext.Current != null)
                        {
                            System.Web.HttpContext.Current.User = principal;
                        }

                    }
                    catch (SignatureVerificationException)
                    {
                        actionResultStatus.code = (int)HttpStatusCode.Unauthorized; 
                        actionResultStatus.data = null;
                        actionResultStatus.message = "Token has invalid signature";
                        actionResultStatus.description = null;

                        return response = request.CreateResponse(HttpStatusCode.Unauthorized, actionResultStatus);

                    }
                }
                else
                {
                    actionResultStatus.code = (int)HttpStatusCode.Unauthorized;
                    actionResultStatus.data = null;
                    actionResultStatus.message = "Unauthorized";
                    actionResultStatus.description = null;

                    return response = request.CreateResponse(HttpStatusCode.Unauthorized, actionResultStatus);

                }
                return response = await base.SendAsync(request, cancellationToken);
            }
        }





    }
}