﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ORApiFramework.Model.Enum
{
    public enum ActionType
    {
        [DefaultValue("Add")]
        Add = 0,

        [DefaultValue("Edit")]
        Edit = 1,

        [DefaultValue("Delete")]
        Delete = 2,

        [DefaultValue("Check")]
        Check = 4,

        [DefaultValue("Get")]
        Get = 5
    }
}