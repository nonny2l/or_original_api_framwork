﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ORApiFramework.Model.Enum
{
    public enum StatusActionResult
    {
        [DefaultValue("INTERNAL BUSSINESS LOGIC")]
        InternalBL = 0,

        [DefaultValue("EXTERNAL BUSSINESS LOGIC")]
        EXternalBL = 1,

    }
}