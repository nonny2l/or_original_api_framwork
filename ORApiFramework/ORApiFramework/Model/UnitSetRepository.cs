﻿using System;
using System.Collections.Generic;

namespace ORApiFramework.Model
{
    public class UnitSetRepository : IDisposable
    {
        private readonly VQPlatFormModelContext context;
        private bool disposed;
        private Dictionary<string, object> repositories;

        public UnitSetRepository(VQPlatFormModelContext context)
        {
            this.context = context;
        }

        public UnitSetRepository()
        {
            context = new VQPlatFormModelContext();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public AbRepository<T> Repositories<T>() where T : class, new()
        {
            if (repositories == null)
            {
                repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (!repositories.ContainsKey(type))
            {
                var repositoryType = typeof(AbRepository<T>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context);
                repositories.Add(type, repositoryInstance);
            }
            return (AbRepository<T>)repositories[type];
        }
    }

}
