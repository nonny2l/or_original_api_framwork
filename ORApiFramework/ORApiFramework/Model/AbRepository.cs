﻿using ORApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace ORApiFramework.Model
{
    /// <summary>
    /// คอนโทรลเลอร์หลักสำหรับจัดการข้อมูลของระบบ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbRepository<T> where T : class, new()
    {
        public AbRepository()
        {
            dbContext = new VQPlatFormModelContext();
            this.data = new T();
            this.datas = new List<T>();
        }
        private VQPlatFormModelContext dbContext;
        private DbContextTransaction dbContextTransaction;
        private T data;
        private List<T> datas;
        private Type typeData;
        private string moduleName;
        //private static readonly ILog log = log4net.LogManager.GetLogger(typeof(T));
        // private LogManager logManager;


        /// <summary>
        /// <see cref="ModelEntities"/> เพื่อจัดการข้อมูลในฐานข้อมูล
        /// </summary>
        protected VQPlatFormModelContext DbContext
        {
            get { return this.dbContext; }
            set { this.dbContext = value; }
        }

        /// <summary>
        /// <see cref="DbContextTransaction"/> ใช้กรณีต้องบันทึกข้อมูลเป็นชุด
        /// </summary>
        protected DbContextTransaction DbContextTransaction
        {
            get { return this.dbContextTransaction; }
            set { this.dbContextTransaction = value; }
        }
        protected string ModuleName
        {
            get { return this.moduleName = "CalculateIncentive"; }
            set { this.moduleName = value; }
        }

        public virtual T Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        public virtual List<T> Datas
        {
            get { return this.datas; }
            set { this.datas = value; }
        }

        protected Type TypeClass
        {
            get { return typeof(T); }
        }


        public virtual bool Create(T data)
        {
            bool result = false;
            this.Save("0", data, ActionType.Add, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();s
            return result;

        }


        public virtual bool Delete(string id, T data)
        {
            bool result = false;
            this.Save(id, data, ActionType.Delete, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();
            return result;

        }


        public virtual bool Edit(string id, T data)
        {
            bool result = false;
            this.Save(id, data, ActionType.Edit, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();
            return result;

        }

        public virtual void SaveLogTransection(string key, ActionType action)
        {
            /// ยังไม่ทราบ Table ที่ใช้งาน
            StackFrame stackFrame = new StackFrame(1, true);
            string[] temp = stackFrame.GetFileName().Split('\\');
            string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
            string functionName = stackFrame.GetMethod().Name;
            int fileLineNumber = stackFrame.GetFileLineNumber();
            int fileColumnNumber = stackFrame.GetFileColumnNumber();
            string className = stackFrame.GetMethod().DeclaringType.Name;

        }
        public virtual void SaveLogErorTransection(string key, ActionType action, Exception exception)
        {

            try
            {
                string resultException = GetEntityErrorException(exception);


                var errorInfo = string.Empty;

                StackFrame stackFrame = new StackFrame(1, true);
                string[] temp = stackFrame.GetFileName().Split('\\');
                string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
                string functionName = stackFrame.GetMethod().Name;
                int fileLineNumber = stackFrame.GetFileLineNumber();
                int fileColumnNumber = stackFrame.GetFileColumnNumber();
                string className = stackFrame.GetMethod().DeclaringType.Name;
                string[] stack = exception.StackTrace.Split('\n');

                errorInfo += "///* Comment *///";
                errorInfo += Environment.NewLine;
                errorInfo += "File Name : " + fileClassName + Environment.NewLine;
                errorInfo += "Class Name : " + className + Environment.NewLine;
                errorInfo += "Method Name : " + functionName + Environment.NewLine;
                errorInfo += "Line Number : " + fileLineNumber + Environment.NewLine;
                errorInfo += "Column Number : " + fileColumnNumber + Environment.NewLine;
                errorInfo += "////////////////";

            }
            catch (Exception ex)
            {
                var err = GetEntityErrorException(ex);
            }
        }


        private static string GetEntityErrorException(Exception exception)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            if (entityException == typeof(DbEntityValidationException))
            {
                resultException += EntityErrorException(exception);
            }
            else
            {
                if (exception.InnerException != null)
                {
                    if (exception.InnerException.InnerException != null)
                    {
                        if (exception.InnerException.InnerException.InnerException != null)
                            resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        else
                            resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                    }
                    else
                    {
                        resultException += exception.InnerException.Message + Environment.NewLine;
                    }
                }
                else
                {
                    resultException += exception.Message + Environment.NewLine;
                }
            }
            return resultException;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }

        /// <summary>
        /// การเรียกดูข้อมูลแบบแถวเดียวโดยต้องส่งรหัสของแถว (id or primary key) มาด้วย
        /// </summary>
        /// <param name="id">
        /// id เป็นรหัสของแถวหรือเป็นคีย์หลัก
        /// </param>
        /// <returns>
        /// กรณีพบข้อมูลตามเงื่อนไขที่กำหนด คืนค่าข้อมูลแบบแถวเดียว,
        /// กรณีอื่นๆ คื่นค่า <b>Null</b> หรือ default
        /// </returns>
        public abstract T GetData(string id);

        public abstract List<T> GetDatas(string id);

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = DbContext.Set<T>();
            return query;
        }

        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = DbContext.Set<T>().Where(predicate);
            return query;
        }

        /// <summary>
        /// การจัดการข้อมูลในฐานข้อมูล(insert, update, delete) ต้องทำผ่าน method นี้ และต้องมี id, data, action และ resultStatus ด้วย
        /// </summary>
        /// <param name="id">
        /// id เป็นรหัสของแถวหรือเป็นคีย์หลัก
        /// </param>
        /// <param name="data">
        /// ข้อมูลที่จะบันทึกเข้าสู่ฐานข้อมูล หรือข้อมูลที่จะลบออกจากฐานข้อมูล
        /// </param>
        /// <param name="action">
        /// ประเภทของการจัดการข้อมูลในฐานข้อมูล
        /// - Add
        /// - Edit
        /// - Delete
        /// </param>
        /// <param name="resultStatus">
        /// สถานะการจัดการข้อมูล 
        /// - กำหนดค่า <b>True</b> กรณีทำรายการสำเร็จ
        /// - กำหนดค่า <b>False</b> กรณีอื่นๆ
        /// </param>
        /// <returns></returns>
        private void Save(string id, T data, ActionType action, out bool resultStatus)
        {
            resultStatus = false;
            try
            {
                switch (action)
                {
                    case ActionType.Add:
                        this.dbContext.Set<T>().Add(data);
                        break;
                    case ActionType.Edit:
                        this.dbContext.Entry(data).State = EntityState.Modified;
                        break;
                    case ActionType.Delete:
                        this.dbContext.Set<T>().Remove(data);
                        break;
                }
                resultStatus = true;
            }
            catch (Exception ex)
            {
                throw new Exception(GetEntityErrorException(ex));
            }
        }
    }
}
