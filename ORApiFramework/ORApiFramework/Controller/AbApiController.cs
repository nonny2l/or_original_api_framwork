﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Security;
using ORApiFramework.Utility.Pagination;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;

namespace ORApiFramework.Controller
{
    public abstract class AbApiController : ApiController
    {
        private List<object> itemsData;
        private object itemData;
        private List<object> filterData;
        private object objfilterData;
        private object sortData;
        private dynamic inputRequest;
        private PaginationRequest inputRequestPaging;
        private VQPlatFormModelContext dbContext;
        private DbContextTransaction dbContextTransaction;
        private bool enblePaging;
        private bool enbleDataList;
        private string methodName;
        private string moduleName;
        private ActionResultStatus resultStatus;


        public AbApiController()
        {
            this.enblePaging = false;
            this.enbleDataList = false;
        }

        /// <summary>
        /// <see cref="ModelEntities"/> เพื่อจัดการข้อมูลในฐานข้อมูล
        /// </summary>
        protected VQPlatFormModelContext DbContext
        {
            get { return this.dbContext = new Model.VQPlatFormModelContext(); }
            set { this.dbContext = value; }
        }

        /// <summary>
        /// <see cref="DbContextTransaction"/> ใช้กรณีต้องบันทึกข้อมูลเป็นชุด
        /// </summary>
        protected DbContextTransaction DbContextTransaction
        {
            get { return this.dbContextTransaction; }
            set { this.dbContextTransaction = value; }
        }

        public abstract string ModuleName { get; }

        public string MethodName
        {
            get { return this.methodName; }
            set { this.methodName = value; }
        }


        public bool EnblePaging
        {
            get { return this.enblePaging; }
            set { this.enblePaging = value; }
        }
        public bool EnbleDataList
        {
            get { return this.enbleDataList; }
            set { this.enbleDataList = value; }
        }

        private static string GetEntityErrorException(Exception exception)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            if (entityException == typeof(DbEntityValidationException))
            {
                resultException += EntityErrorException(exception);
            }
            else
            {
                if (exception.InnerException != null)
                {
                    if (exception.InnerException.InnerException != null)
                    {
                        if (exception.InnerException.InnerException.InnerException != null)
                            resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        else
                            resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                    }
                    else
                    {
                        resultException += exception.InnerException.Message + Environment.NewLine;
                    }
                }
                else
                {
                    resultException += exception.Message + Environment.NewLine;
                }
            }
            return resultException;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }

        public virtual bool InputRequest(object inputRequest)
        {
            bool result = false;
            this.inputRequest = inputRequest;
            if (this.inputRequest != null)
                result = true;

            return result;
        }

        //public virtual bool InputRequestPaging(PaginationRequest inputRequestPaging)
        //{
        //    bool result = false;
        //    this.inputRequestPaging = inputRequestPaging;
        //    if (this.inputRequestPaging != null)
        //        result = true;

        //    return result;
        //}

        //public virtual bool SetItems(List<object> itemsData)
        //{
        //    bool result = false;
        //    this.itemsData = itemsData;
        //    if (this.itemsData != null)
        //        result = true;

        //    return result;
        //}
        //// data ที่เป็น object
        //public virtual bool SetItems(object itemsData)
        //{
        //    bool result = false;
        //    this.itemData = itemsData;
        //    if (this.itemData != null)
        //        result = true;

        //    return result;
        //}

        public virtual bool SetFilter(List<object> filterData)
        {
            bool result = false;
            this.filterData = filterData;
            if (this.filterData != null)
                result = true;

            return result;
        }

        // data ที่เป็น object
        public virtual bool SetFilter(object filterData)
        {
            bool result = false;
            this.objfilterData = filterData;
            if (this.objfilterData != null)
                result = true;

            return result;
        }

        public virtual bool SetSorting(object sortData)
        {
            bool result = false;
            this.sortData = sortData;
            if (this.sortData != null)
                result = true;

            return result;
        }

        public virtual IHttpActionResult SendResponse(ActionResultStatus actionResultStatus)
        {
            APIResultResponseListPaging apiResultListPaging = new APIResultResponseListPaging();
            APIResultResponse apiResultList = new APIResultResponse();

            try
            {

                //var requestBody = JsonConvert.DeserializeObject<object>(RequestBody());
                //Save_log.SaveLogObject(this.inputRequest, JObject.Parse(JsonConvert.SerializeObject(requestBody)), "SaveRequest", methodName);//save log output

                if (this.enblePaging)
                {
                    if (this.filterData == null)
                        this.filterData = new List<object>();

                    apiResultListPaging.success = actionResultStatus.success;
                    apiResultListPaging.code = actionResultStatus.code.ToString();
                    apiResultListPaging.message = actionResultStatus.message;
                    apiResultListPaging.description = actionResultStatus.description;
                    apiResultListPaging.key = JwtManagement.GenerateJasonWebToken();

                    PropertyInfo pagination = this.inputRequest.GetType().GetProperty("pagination");
                    this.inputRequestPaging = (PaginationRequest)(pagination.GetValue(this.inputRequest, null));

                    if (this.inputRequestPaging == null)
                    {
                        apiResultList.success = false;
                        apiResultList.code = actionResultStatus.code.ToString();
                        apiResultList.message = "Can not inherit PaginiationMaster on class request";
                        apiResultList.description = actionResultStatus.description;
                    }


                    if (actionResultStatus.filter != null)
                        apiResultListPaging.filters.Add(actionResultStatus.filter);

                    if (actionResultStatus.data != null)
                        apiResultListPaging.items.Add(actionResultStatus.data);

                    if (actionResultStatus.otherData != null)
                        apiResultListPaging.otherItems = actionResultStatus.otherData;
                    else
                    {
                        if (WebConfigurationManager.AppSettings["EnableRequestObjectOther"].ToString() == "Y")
                            apiResultListPaging.otherItems = this.inputRequest;
                    }

                    var responseResult = apiResultListPaging.ResponseResult(this.Request, apiResultListPaging.items, ModelState, this.inputRequestPaging, this.inputRequest, this.moduleName, this.methodName);
                    return ResponseMessage(responseResult);

                }
                else
                {

                    apiResultList.success = actionResultStatus.success;
                    apiResultList.key = JwtManagement.GenerateJasonWebToken();
                    apiResultList.code = actionResultStatus.code.ToString();
                    apiResultList.message = actionResultStatus.message;
                    apiResultList.description = actionResultStatus.description;

                    if (actionResultStatus.data != null)
                        apiResultList.items = actionResultStatus.data;

                    if (actionResultStatus.otherData != null)
                        apiResultList.otherItems = actionResultStatus.otherData;

                    var responseResult = HttpManageResponse.Response.ResponseResultList(this.Request, apiResultList,ModelState, this.inputRequest, ModuleName, methodName);
                    return ResponseMessage(responseResult);
                }

            }
            catch (Exception ex)
            {
                if (this.enblePaging)
                {
                    var response = HttpManageResponse.Response.ResponseMessage(this.Request, Model.Enum.StatusCode.EROR, ex.Message.ToString(), apiResultListPaging);
                    SaveLog.SaveLogObject(apiResultListPaging, this.inputRequest, ModuleName, methodName);//save log output
                    return ResponseMessage(response);
                }
                else
                {
                    var response = HttpManageResponse.Response.ResponseMessage(this.Request, Model.Enum.StatusCode.EROR, ex.Message.ToString(), apiResultList);
                    SaveLog.SaveLogObject(apiResultList, this.inputRequest, ModuleName, methodName);//save log output
                    return ResponseMessage(response);
                }


            }
        }
    }
}
